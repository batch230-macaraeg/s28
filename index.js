// with missing data which is price
db.hotel.insertOne({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all basic necessities",
	rooms_available: 10,
	isAvailable: false
})

db.hotel.insertMany(
		[
		{	
			name: "double",
			accomodates: 3,
			price: 2000,
			description: "A room fit for a small family going on a vacation",
			rooms_available: 5,
			isAvailable: false
		},
		{
			name: "queen",
			accomodates: 4,
			price: 4000,
			description: "A room with a queen sized bed perfect for a simple getaway",
			rooms_available: 15,
			isAvailable: false
		}

	]
)

db.hotel.find({name: "double"})

db.hotel.updateOne(
	{name: "queen"},
		{
			$set:{
			name: "queen",
			accomodates: 0,
			price: 4000,
			description: "A room with a queen sized bed perfect for a simple getaway",
			rooms_available: 15,
			isAvailable: false
		}

	}

)

db.hotel.deleteMany({
	accomodates:0,
})
db.hotel.deleteMany({
	name: "single",
});

db.hotel.insertOne({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all basic necessities",
	rooms_available: 10,
	isAvailable: false
})